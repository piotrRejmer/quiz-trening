<?php

namespace My\AkcjeBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use My\AkcjeBundle\Entity\Quiz;
use My\AkcjeBundle\Entity\QuizQuestion;
use My\AkcjeBundle\Form\QuizQuestionsType;
use My\AkcjeBundle\Form\QuizType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class QuizController extends Controller
{
    /**
     * Create new quiz
     *
     * @Route("/quiz/new", name="new_quiz")
     *
     */
    public function quizNewAction(Request $request)
    {
        $quiz = new Quiz();
        $form = $this->createForm(QuizType::class, $quiz);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //TODO: zmienic na dql wlasna funkcja random
            $rsm = $this->getResultSetMapping();
            // native sql
            $query = $em->createNativeQuery('SELECT id FROM questions WHERE level = ? AND isActive = 1 ORDER BY RAND() LIMIT ?', $rsm);
            $query->setParameter(1, $quiz->getLevel());
            $query->setParameter(2, $quiz->getIloscQuestions());
            $questions = $query->getResult();

            // if not enough questions
            if(count($questions) < $quiz->getIloscQuestions()){
                return $this->render('quiz/notEnoughQuestions.html.twig',array(
                    'iloscZform' => $quiz->getIloscQuestions(),
                    'questions' => $questions,
                ));
            }
            $quiz->setWylosowaneQuestions($questions);

            foreach($questions as $question) {
                $quizQuestion= new QuizQuestion();
                $quizQuestion->setQuestion($question);
                $quizQuestion->setQuiz($quiz);

                //add goodAnswer in quizQuestion
                $answers = $question->getAnswers();
                foreach ($answers as $answer )
                {
                    if(1 == $answer->getCorrectAnswer()) {
                        $goodAnswer['odp'] = $answer->getId();
                    }
                }
                $quizQuestion->setGoodAnswer($goodAnswer['odp']);

                $quiz->addQuizQuestion($quizQuestion);
            }
            $em->persist($quiz);
            $em->flush();
            return $this->redirectToRoute('all_quiz',array('id'=>$quiz->getId()));
        }

        return $this->render('quiz/new.html.twig',array(
            'form'=>$form->createView(),
        ));
    }



    /**
     * All questions from Quiz
     *
     * @Route("/quiz/all/{id}/{page}",
     *      name="all_quiz",
     *      defaults = {"page" =1},
     *     requirements={"id": "\d+"}
     *     )
     *
     */
    public function quizAllAction(Request $request, Quiz $quiz, $page)
    {
       if ($page < 1 or $page > $quiz->getIloscQuestions()){
           throw $this->createNotFoundException('dana strona nie istnieje');
       }
        $Questions = $quiz->getQuestions();
        $question=$Questions[$page-1];

        $oneForm = $this->createForm(QuizQuestionsType::class,null,array('question'=>$question));

            $oneForm->handleRequest($request);
            if ($oneForm->isSubmitted() && $oneForm->isValid()) {
                $selectedAnswer = $oneForm->getData();
                $quiz->setSelectedAnswer($question, $selectedAnswer['odp']);
                $em = $this->getDoctrine()->getManager();
                $em->persist($quiz);
                $em->flush();
                if($page < ($quiz->getIloscQuestions())){
                     return $this->redirectToRoute('all_quiz',array('id'=>$quiz->getId(),'page'=>$page+1));
                    }
                else {
                    return $this->redirectToRoute('score',array('id'=>$quiz->getId()));
                }
            }

        return $this->render('quiz/one.html.twig', array(
            'question' => $question,
            'form' => $oneForm->createView(),
            'quiz'=>$quiz,
        ));
    }


    /**
     * score a quiz
     *
     * @Route("/quiz/score/{id}", name="score")
     *
     */
    public function ScoreAction(Request $request, Quiz $quiz)
    {
        $QuizQuestionRepo = $this->getDoctrine()->getRepository('MyAkcjeBundle:QuizQuestion');
        $QuizQuestions = $QuizQuestionRepo->findByQuiz($quiz->getId());
        $iloscGoodAnswer =0;
        foreach($QuizQuestions as $question) {
            if ($question->getSelectedAnswer() == $question->getGoodAnswer()) {
                $iloscGoodAnswer++;
            }
        }

        return $this->render('quiz/score.html.twig',array(
            'iloscGoodAnswer'=>$iloscGoodAnswer,
            'quizQuestions'=>$QuizQuestions
        ));
    }
protected function getResultSetMapping(){
    // ResultSetMapping
    $rsm = new ResultSetMapping;
    $rsm->addEntityResult('MyAkcjeBundle:Question', 'qu');
    $rsm->addFieldResult('qu', 'id', 'id');
    $rsm->addFieldResult('qu', 'questionText', 'questionText');
    $rsm->addFieldResult('qu', 'level', 'level');
    $rsm->addFieldResult('qu', 'isActive', 'isActive');

    return $rsm;

}
}
