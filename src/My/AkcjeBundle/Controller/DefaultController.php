<?php

namespace My\AkcjeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     *
     * @Route("/", name="info")
     */
    public function indexAction()
    {
        return $this->render('MyAkcjeBundle:Default:index.html.twig');
    }


}
