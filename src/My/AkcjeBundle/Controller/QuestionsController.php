<?php

namespace My\AkcjeBundle\Controller;

use My\AkcjeBundle\Entity\Answer;
use My\AkcjeBundle\Form\AnswersType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use My\AkcjeBundle\Entity\Question;
use My\AkcjeBundle\Form\QuestionsType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session;
/**
 * Questions controller
 *
 * @Route("/questions")
 */
class QuestionsController extends Controller
{
    /**
     * all questions with Pagination
     *
     * @Route(
     *           "/questions_pagination/{page}",
     *           name="questions_pagination",
     *           defaults = {"page" =1}
     *  )
     *
     */
    public function questions_paginationAction(Request $request, $page)
    {
        $em = $this->getDoctrine()->getManager();
        $questions = $em->getRepository('MyAkcjeBundle:Question')->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $questions, /* query NOT result */
            $page/*page number*/,
            2/*limit per page*/
        );

        return $this->render('questions/pagination.html.twig', array('pagination' => $pagination));
    }

    /**
     * create new question
     *
     * @Route("/new", name="questions_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $question = new Question();
        $form = $this->createForm(QuestionsType::class, $question);
        $form->handleRequest($request);
        $Session = $this->get('session');

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();
            $Session->getFlashBag()->add('success','Dane pytanie  zostało dodane');

            return $this->redirectToRoute('questions_pagination', array('id' => $question->getId()));
        }
        return $this->render('questions/new.html.twig', array(
            'question' => $question,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a questions entity.
     *
     * @Route("/{id}", name="questions_show", requirements={"id": "\d+"})
     *
     * @Method("GET")
     */
    public function showAction(Question $question)
    {

        $deleteForm = $this->createDeleteForm($question);

        return $this->render('questions/show.html.twig', array(
            'question' => $question,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing questions entity.
     *
     * @Route("/{id}/edit", name="questions_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Question $question)
    {
        $answer = new Answer();
        $addAnswerForm=$this->createForm(AnswersType::class, $answer);
        $deleteForm = $this->createDeleteForm($question);
        $editForm = $this->createForm(QuestionsType::class, $question);
        $editForm->handleRequest($request);
        $addAnswerForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();

            return $this->redirectToRoute('questions_pagination', array('id' => $question->getId()));
        }
        if ($addAnswerForm->isSubmitted() && $addAnswerForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $answer->setQuestions($question);
            $em->persist($answer);
            $em->flush();
            return $this->redirectToRoute('questions_pagination', array('id' => $question->getId()));
        }

        return $this->render('questions/edit.html.twig', array(
            'question' => $question,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'add_Answer_form'=>$addAnswerForm->createView(),
        ));
    }

    /**
     * Deletes a questions entity.
     *
     * @Route("/{id}", name="questions_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Question $question)
    {
        $form = $this->createDeleteForm($question);
        $form->handleRequest($request);
        $Session = $this->get('session');


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($question);
            $em->flush();
            $Session->getFlashBag()->add('success','Dane pytanie zostało usuniete');
        }
        else {
            $Session->getFlashBag()->add('danger','Dane pytanie nie zostało usuniete');

        }
        return $this->redirectToRoute('questions_pagination');
    }

    /**
     * Creates a form to delete a questions entity.
     *
     * @param Question $question The questions entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Question $question)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('questions_delete', array('id' => $question->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
