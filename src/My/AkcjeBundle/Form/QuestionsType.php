<?php

namespace My\AkcjeBundle\Form;

use My\AkcjeBundle\Form\AnswersType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class QuestionsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('questionName',TextType::class,array('label'=>'Question name'))
            ->add('questionText',TextType::class,array('label'=>'Question text'))
            ->add('isMany',ChoiceType::class, array('label'=>'Multiple','choices'=>array('no'=>'0','yes'=>'1')))
            ->add('level',ChoiceType::class, array('label'=>'Level','choices'=>array('basic'=>'1','advanced'=>'2','expert'=>'3')))
            ->add('isActive',ChoiceType::class, array('label'=>'Active','choices'=>array('yes'=>'1','no'=>'0',)))
        ->add('answers',CollectionType::class, array('entry_type' => AnswersType::class
        ));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'My\AkcjeBundle\Entity\Question'
        ));
    }
}
