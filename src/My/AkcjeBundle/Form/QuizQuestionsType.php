<?php

namespace My\AkcjeBundle\Form;

use My\AkcjeBundle\Entity\Answer;
use My\AkcjeBundle\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class QuizQuestionsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $question=$options['question'];
                    $answerChoices = array();
                    foreach ($question->getAnswers() as $answer) {
                        if ($answer instanceof Answer) {
                            $answerChoices[$answer->getAnswerText()] =$answer->getId() ;
                        }
                    }

        $builder->add('odp', ChoiceType::class, array('label' => $question->getQuestionText(),'expanded' => true, 'choices' => $answerChoices, 'multiple' => $question->getIsMany()))
            ->add('save', SubmitType::class, array('label' => 'Save&Next'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array('question'));
    }

}
