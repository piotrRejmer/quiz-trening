<?php

/**
 * Created by PhpStorm.
 * User: rejki
 * Date: 02.02.16
 * Time: 10:05
 */

namespace My\AkcjeBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use My\AkcjeBundle\Entity\Answer;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
class AnswersFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $answersList = array(
            array(
                'QuestionName'=>'Programowanie pyt1',
                'answerText'=>'Kompilator',
                'correctAnswer'=>true,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Programowanie pyt1',
                'answerText'=>'Debugger',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Programowanie pyt1',
                'answerText'=>'Środowisko programistyczne',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Grafika pyt1',
                'answerText'=>'JPEG',
                'correctAnswer'=>true,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Grafika pyt1',
                'answerText'=>'PCX',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Grafika pyt1',
                'answerText'=>'GIF',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'CSS pyt1',
                'answerText'=>'ułatwienia formatowania strony',
                'correctAnswer'=>true,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'CSS pyt1',
                'answerText'=>'nadpisywania wartości znaczników już ustawionych na stronie',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'CSS pyt1',
                'answerText'=>'połączenia struktury dokumentu strony z właściwą formą jego prezentacji',
                'correctAnswer'=>false,
                'question'=>false,
            ),

            array(
                'QuestionName'=>'WYSIWYG pyt1',
                'answerText'=>'tworzenie podstawowej grafiki wektorowej',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'WYSIWYG pyt1',
                'answerText'=>'publikację strony na serwerze poprzez wbudowanego klienta FTP',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'WYSIWYG pyt1',
                'answerText'=>'uzyskanie zbliżonego wyniku tworzenej strony do jej obrazu w przegladarce interenetowej',
                'correctAnswer'=>true,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Walidacja pyt2',
                'answerText'=>'źródła pochodzenia narzędzi edytorskich',
                'correctAnswer'=>true,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Walidacja pyt2',
                'answerText'=>'działania linków',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            array(
                'QuestionName'=>'Walidacja pyt2',
                'answerText'=>'błędów składni kodu',
                'correctAnswer'=>false,
                'question'=>false,
            ),
            );

        foreach ($answersList as $details) {
            $answers = new Answer();
            $answers->setAnswerText($details['answerText']);
            $answers->setCorrectAnswer($details['correctAnswer']);
            $answers->setQuestions($this->getReference('question_'.$details['QuestionName']));
            $manager->persist($answers);

        }


             $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}