<?php

/**
 * Created by PhpStorm.
 * User: rejki
 * Date: 02.02.16
 * Time: 10:05
 */

namespace My\AkcjeBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use My\AkcjeBundle\Entity\Question;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class QuestionsFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $questionsList = array(
            array(
                'QuestionName'=>'Programowanie pyt1',
                'QuestionText'=>'Jaki program komputerowy przekształca kod źródłowy, napisany w konkretnym języku programowania, na język komputera?',
                'IsMany'=>false,
                'IsActive'=>false,
                'Level'=>1

            ),
            array(
                'QuestionName'=>'Grafika pyt1',
                'QuestionText'=>'Jakiego formatu należy użyć do zapisu obrazu z kompresją stratną?',
                'IsMany'=>false,
                'IsActive'=>false,
                'Level'=>1

            ),
            array(
                'QuestionName'=>'CSS pyt1',
                'QuestionText'=>'Kaskadowe arkusze stylów tworzy się w celu',
                'IsMany'=>false,
                'IsActive'=>true,
                'Level'=>1

            ),
            array(
                'QuestionName'=>'Walidacja pyt2',
                'QuestionText'=>' W procesie walidacji stron internetowych nie bada się',
                'IsMany'=>false,
                'IsActive'=>true,
                'Level'=>1

            ),
            array(
                'QuestionName'=>'WYSIWYG pyt1',
                'QuestionText'=>'Edytor spełniający założenia WYSIWYG musi umożliwiać',
                'IsMany'=>false,
                'IsActive'=>true,
                'Level'=>1
            ));

        foreach ($questionsList as $details)
        {
            $question= new Question();
            $question->setQuestionName($details['QuestionName']);
            $question->setQuestionText($details['QuestionText']);
            $question->setIsMany($details['IsMany']);
            $question->setIsActive($details['IsActive']);
            $question->setLevel($details['Level']);
            $manager->persist($question);

            $this->addReference('question_'.$details['QuestionName'],$question);
        }


        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}