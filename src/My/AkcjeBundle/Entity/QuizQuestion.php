<?php

namespace My\AkcjeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quiz
 *
 * @ORM\Table(name="quiz_question")
 * @ORM\Entity(repositoryClass="My\AkcjeBundle\Repository\QuizRepository")
 */
class QuizQuestion
{

    /**
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="quizQuestions")
     * @ORM\Id
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="Quiz", inversedBy="quizQuestions")
     * @ORM\Id
     */
    private $quiz;

    /**
     * @var int
     *
     * @ORM\Column(name="selected_answer", type="integer", nullable=true)
     */
    private $selectedAnswer;

    /**
     * @var int
     *
     * @ORM\Column(name="good_answer", type="integer", nullable=true)
     */
    private $goodAnswer;
    


    /**
     * Set selectedAnswer
     *
     * @param integer $selectedAnswer
     *
     * @return QuizQuestion
     */
    public function setSelectedAnswer($selectedAnswer)
    {
        $this->selectedAnswer = $selectedAnswer;

        return $this;
    }

    /**
     * Get selectedAnswer
     *
     * @return integer
     */
    public function getSelectedAnswer()
    {
        return $this->selectedAnswer;
    }

    /**
     * Set question
     *
     * @param \My\AkcjeBundle\Entity\Question $question
     *
     * @return QuizQuestion
     */
    public function setQuestion(\My\AkcjeBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \My\AkcjeBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set quiz
     *
     * @param \My\AkcjeBundle\Entity\Quiz $quiz
     *
     * @return QuizQuestion
     */
    public function setQuiz(\My\AkcjeBundle\Entity\Quiz $quiz)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz
     *
     * @return \My\AkcjeBundle\Entity\Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * Set goodAnswer
     *
     * @param integer $goodAnswer
     *
     * @return QuizQuestion
     */
    public function setGoodAnswer($goodAnswer)
    {
        $this->goodAnswer = $goodAnswer;

        return $this;
    }

    /**
     * Get goodAnswer
     *
     * @return integer
     */
    public function getGoodAnswer()
    {
        return $this->goodAnswer;
    }
}
