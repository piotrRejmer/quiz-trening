<?php

namespace My\AkcjeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;   //do walidacji encji

/**
 * Question
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="My\AkcjeBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="questionName", type="string", length=255, unique=true)
     */
    private $questionName;

    /**
     * @var string
     *
     * @ORM\Column(name="questionText", type="string", length=255)
     *
     * @Assert\NotBlank
     */
    private $questionText;

    /**
     * @var int
     *
     * @ORM\Column(name="isMany", type="boolean")
     */
    private $isMany;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;
    /**
     * samemu dodalem
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="questions",cascade={"all"})
     */
    protected $answers;

    /**
     * @ORM\OneToMany(targetEntity="QuizQuestion", mappedBy="question")
     *
     */
    private $quizQuestions;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionName
     *
     * @param string $questionName
     *
     * @return Question
     */
    public function setQuestionName($questionName)
    {
        $this->questionName = $questionName;

        return $this;
    }

    /**
     * Get questionName
     *
     * @return string
     */
    public function getQuestionName()
    {
        return $this->questionName;
    }

    /**
     * Set isMany
     *
     * @param boolean $isMany
     *
     * @return Question
     */
    public function setIsMany($isMany)
    {
        $this->isMany = $isMany;

        return $this;
    }

    /**
     * Get isMany
     *
     * @return boolean
     */
    public function getIsMany()
    {
        return $this->isMany;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Question
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Question
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add answer
     *
     * @param \My\AkcjeBundle\Entity\Answer $answer
     *
     * @return Question
     */
    public function addAnswer(\My\AkcjeBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \My\AkcjeBundle\Entity\Answer $answer
     */
    public function removeAnswer(\My\AkcjeBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function getQuestionText()
    {
        return $this->questionText;
    }

    public function setQuestionText($questionText)
    {
        $this->questionText = $questionText;
    }

    /**
     * Add quiz
     *
     * @param \My\AkcjeBundle\Entity\Quiz $quiz
     *
     * @return Question
     */
    public function addQuiz(\My\AkcjeBundle\Entity\Quiz $quiz)
    {
        $this->quizs[] = $quiz;

        return $this;
    }

    /**
     * Remove quiz
     *
     * @param \My\AkcjeBundle\Entity\Quiz $quiz
     */
    public function removeQuiz(\My\AkcjeBundle\Entity\Quiz $quiz)
    {
        $this->quizs->removeElement($quiz);
    }

    /**
     * Get quizs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizs()
    {
        return $this->quizs;
    }

    /**
     * Add quizQuestion
     *
     * @param \My\AkcjeBundle\Entity\QuizQuestion $quizQuestion
     *
     * @return Question
     */
    public function addQuizQuestion(\My\AkcjeBundle\Entity\QuizQuestion $quizQuestion)
    {
        $this->quizQuestions[] = $quizQuestion;

        return $this;
    }

    /**
     * Remove quizQuestion
     *
     * @param \My\AkcjeBundle\Entity\QuizQuestion $quizQuestion
     */
    public function removeQuizQuestion(\My\AkcjeBundle\Entity\QuizQuestion $quizQuestion)
    {
        $this->quizQuestions->removeElement($quizQuestion);
    }

    /**
     * Get quizQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizQuestions()
    {
        return $this->quizQuestions;
    }
}
