<?php

namespace My\AkcjeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * answers
 *
 * @ORM\Table(name="answers")
 * @ORM\Entity(repositoryClass="My\AkcjeBundle\Repository\answersRepository")
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="answerText", type="string", length=255)
     */
    private $answerText;

    /**
     * @var bool
     *
     * @ORM\Column(name="correctAnswer", type="boolean")
     */
    private $correctAnswer;
    /**
     * samemu dodalem
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
     */
    protected $questions;

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answerText
     *
     * @param string $answerText
     *
     * @return Answer
     */
    public function setAnswerText($answerText)
    {
        $this->answerText = $answerText;

        return $this;
    }

    /**
     * Get answerText
     *
     * @return string
     */
    public function getAnswerText()
    {
        return $this->answerText;
    }

    /**
     * Set correctAnswer
     *
     * @param boolean $correctAnswer
     *
     * @return Answer
     */
    public function setCorrectAnswer($correctAnswer)
    {
        $this->correctAnswer = $correctAnswer;

        return $this;
    }

    /**
     * Get correctAnswer
     *
     * @return boolean
     */
    public function getCorrectAnswer()
    {
        return $this->correctAnswer;
    }

    /**
     * Set questions
     *
     * @param \My\AkcjeBundle\Entity\Question $questions
     *
     * @return Answer
     */
    public function setQuestions(\My\AkcjeBundle\Entity\Question $questions = null)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return \My\AkcjeBundle\Entity\Question
     */
    public function getQuestions()
    {
        return $this->questions;
    }
}
