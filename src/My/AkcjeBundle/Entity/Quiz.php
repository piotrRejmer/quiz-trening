<?php

namespace My\AkcjeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \My\AkcjeBundle\Entity\Question;
use My\AkcjeBundle\Entity\QuizQuestion;

/**
 * Quiz
 *
 * @ORM\Table(name="quiz")
 * @ORM\Entity(repositoryClass="My\AkcjeBundle\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userName", type="string", length=255)
     */
    private $userName;

    /**
     * @var int
     *
     * @ORM\Column(name="iloscQuestions", type="integer")
     */
    private $iloscQuestions;

    /**
     * @var array
     *
     * @ORM\Column(name="wylosowaneQuestions", type="array")
     */
    private $wylosowaneQuestions;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="QuizQuestion", mappedBy="quiz", cascade={"all"})
     *
     */
    private $quizQuestions;
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quizQuestions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get questions
     * Metoda napisana w celu pobrania zquizQuestions do tablicy pytan
     */
    public function getQuestions()
    {
        $questions=array();
        foreach($this->quizQuestions as $quizQuestion){
        $questions[]=$quizQuestion->getQuestion();

    }
        return $questions;
    }

    /**
     * Get selectedAnswer
     * Metoda napisana w celu pobrania zquizQuestions selectedAnswer
     */
    public function getselectedAnswer($question)
    {
      //  $questions=array();
        foreach($this->quizQuestions as $quizQuestion){
            if($quizQuestion->getQuestion() == $question){
                $selectedAnswer = $quizQuestion->getSelectedAnswer();
            }

        }
        return $selectedAnswer;
    }

    /**
     * Set selectedAnswer
     * Metoda przeciaza setSelectedAnswer z entity QuizQuestion
     */
    public function setSelectedAnswer($question, $selectedAnswer)
    {

        foreach($this->quizQuestions as $quizQuestion){
            if($quizQuestion->getQuestion() == $question){
                $quizQuestion->setSelectedAnswer($selectedAnswer);
            }

        }
        return $this;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return Quiz
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set iloscQuestions
     *
     * @param integer $iloscQuestions
     *
     * @return Quiz
     */
    public function setIloscQuestions($iloscQuestions)
    {
        $this->iloscQuestions = $iloscQuestions;

        return $this;
    }

    /**
     * Get iloscQuestions
     *
     * @return integer
     */
    public function getIloscQuestions()
    {
        return $this->iloscQuestions;
    }

    /**
     * Set wylosowaneQuestions
     *
     * @param array $wylosowaneQuestions
     *
     * @return Quiz
     */
    public function setWylosowaneQuestions($wylosowaneQuestions)
    {
        $this->wylosowaneQuestions = $wylosowaneQuestions;

        return $this;
    }

    /**
     * Get wylosowaneQuestions
     *
     * @return array
     */
    public function getWylosowaneQuestions()
    {
        return $this->wylosowaneQuestions;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Quiz
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add quizQuestion
     *
     * @param \My\AkcjeBundle\Entity\QuizQuestion $quizQuestion
     *
     * @return Quiz
     */
    public function addQuizQuestion(\My\AkcjeBundle\Entity\QuizQuestion $quizQuestion)
    {
        $this->quizQuestions[] = $quizQuestion;

        return $this;
    }

    /**
     * Remove quizQuestion
     *
     * @param \My\AkcjeBundle\Entity\QuizQuestion $quizQuestion
     */
    public function removeQuizQuestion(\My\AkcjeBundle\Entity\QuizQuestion $quizQuestion)
    {
        $this->quizQuestions->removeElement($quizQuestion);
    }

    /**
     * Get quizQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizQuestions()
    {
        return $this->quizQuestions;
    }
}
